﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace car_fleet
{
    public partial class WorkerAddForm : Form
    {
        public WorkerAddForm()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void WorkerAddForm_Load(object sender, EventArgs e)
        {
            if (WorkerManager.isEditWorker)
            {
                Worker worker = WorkerManager.CurrentWorker;
                label1.Text = $"EDIT WORKER {worker.Id}";
                tbFirstName.Text = worker.FirstName;
                tbLastName.Text = worker.LastName;
                tbEmail.Text = worker.Email;
                tbPhone.Text = worker.Phone;
                tbSalary.Text = worker.Salary;
                comboBox1.Items.Add(worker.Region+"");
                button1.Text = "EDIT";
            }
            else
            {
                label1.Text = "ADD WORKER";
                tbFirstName.Clear();
                tbLastName.Clear();
                tbEmail.Clear();
                tbPhone.Clear();
                tbSalary.Clear();
                comboBox1.Items.Clear();
                MessageBox.Show("add");
                button1.Text = "ADD";

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(WorkerManager.CheckWorker(tbFirstName.Text,tbLastName.Text,tbPhone.Text,tbEmail.Text,tbSalary.Text))
            {
                Worker worker = new Worker(114,tbFirstName.Text, tbLastName.Text, tbPhone.Text, WorkerManager.CreateMail(tbFirstName.Text,tbLastName.Text), tbSalary.Text, 1);
                WorkerManager.AddNewWorker(worker);
                MessageBox.Show($"Added new worker {tbFirstName.Text}");
              /*  var workerList = new WorkerList();
                workerList.Location = this.Location;
                workerList.StartPosition = FormStartPosition.Manual;
                workerList.FormClosing += delegate { this.Show(); };
                workerList.Show();
                this.Hide();*/
            }
           
        }

        private void tbPhone_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
