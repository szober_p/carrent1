﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace car_fleet
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void btnWorkerList_Click(object sender, EventArgs e)
        {
            var workerList = new WorkerList();
            workerList.Location = this.Location;
            workerList.StartPosition = FormStartPosition.Manual;
            workerList.FormClosing += delegate { this.Show(); };
            workerList.Show();
            this.Hide();
        }
    }
}
