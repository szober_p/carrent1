﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace car_fleet
{
    public class WorkerManager
    {
        public static List<String> RegionName = new List<string>()
        {   "All",
            "Poznań",
            "Wrocław",
            "Warszawa"
        };
        public static bool isEditWorker { get; set; }
        public static Worker CurrentWorker { get; set; }
        public static List<Worker> Workers { get; set; }
        public static ListView LvWorker { get; set; }
        public WorkerManager(ListView lvWorker)
        {
            isEditWorker = false;
            LvWorker = lvWorker;
            StreamReader r = new StreamReader("database.json");
            string json = r.ReadToEnd();
            var app = JsonConvert.DeserializeObject<DataFromJson>(json);
            Workers = app.Workers;
            r.Close();
            r.Dispose();
            LoadWorkerListProperty();
            LoadDataToWorkerListView(Workers);
        }

        public static bool CheckWorker(string fn,string ln,string phone,string email, string salary)
        {
            if (String.IsNullOrEmpty(fn))
            {
                MessageBox.Show("Imie jest puste");
                return false;
            }
            if (String.IsNullOrEmpty(ln))
            {
                MessageBox.Show("nazwisko jest puste");
                return false;
            }
            if (String.IsNullOrEmpty(phone))
            {
                MessageBox.Show("phone jest puste");
                return false;
            }
            if (String.IsNullOrEmpty(email))
            {
                MessageBox.Show("email jest puste");
                return false;
            }
            if (String.IsNullOrEmpty(fn))
            {
                MessageBox.Show("salary jest puste");
                return false;
            }
            return true;


        }

        public static void FindWorker(int id)
        {
            Worker current = Workers.Find(e => e.Id == id);
            CurrentWorker = current;
            isEditWorker = true;
        }

        public static void LoadDataFromFilter(string filter)
        {
            LvWorker.Clear();
            LoadWorkerListProperty();

            if (filter == "All")
            {
                LoadDataToWorkerListView(Workers);
                return;
            }

            List<Worker> filterWorker = new List<Worker>();

           

            foreach (var worker in Workers)
            {
                if(RegionName[worker.Region] == filter)
                {
                    filterWorker.Add(worker);
                }
            }
            
            LoadDataToWorkerListView(filterWorker);
        }
        public static string CreateMail(string first, string last)
        {
            return $"{first[0]}{last}@carfleet.com";
        }
        public static void LoadDataToWorkerListView(List<Worker> data)
        {
            string[] arr = new string[12];
            
            foreach (var obj in data)
            {
                arr[0] = obj.Id + "";
                arr[1] = obj.FirstName;
                arr[2] = obj.LastName;
                arr[3] = CreateMail(obj.FirstName, obj.LastName).ToLower();
                arr[4] = obj.Phone;
                arr[5] = RegionName[obj.Region];

                var item = new ListViewItem(arr);
                LvWorker.Items.Add(item);
            }


        }
        public static void LoadWorkerListProperty()
        {
            LvWorker.View = View.Details;
            LvWorker.GridLines = true;
            LvWorker.FullRowSelect = true;
            LvWorker.AllowColumnReorder = true;
           /* LvWorker.Sorting = SortOrder.Ascending;*/
           LvWorker.Columns.Add("ID", 50);
            LvWorker.Columns.Add("First Name", 100);
            LvWorker.Columns.Add("Last Name", 100);
            LvWorker.Columns.Add("Email", 300);
            LvWorker.Columns.Add("Phone", 200);
            LvWorker.Columns.Add("Region", 100);
           

        }

        public static void AddNewWorker(Worker newWorker)
        {
            Workers.Add(newWorker);
            LoadDataToWorkerListView(Workers);
        }

    }
}
