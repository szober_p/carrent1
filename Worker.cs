﻿using System;
using System.Collections.Generic;
using System.Text;

namespace car_fleet
{
    public class Worker
    {
        public Worker(int id, string firstName, string lastName, string email, string phone,string salary, int region)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Phone = phone;
            Salary = salary;
            Region = region;
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Salary { get; set; }
        public int Region { get; set; }

    }
}

