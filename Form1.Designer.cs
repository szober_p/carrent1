﻿namespace car_fleet
{
    partial class Menu
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Menu));
            this.btnCarSection = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnLogOut = new System.Windows.Forms.Button();
            this.btnOrdersList = new System.Windows.Forms.Button();
            this.btnWorkerList = new System.Windows.Forms.Button();
            this.btnReturnsList = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCarSection
            // 
            this.btnCarSection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnCarSection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCarSection.Font = new System.Drawing.Font("Fira Code", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnCarSection.Image = ((System.Drawing.Image)(resources.GetObject("btnCarSection.Image")));
            this.btnCarSection.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnCarSection.Location = new System.Drawing.Point(12, 46);
            this.btnCarSection.Name = "btnCarSection";
            this.btnCarSection.Size = new System.Drawing.Size(626, 300);
            this.btnCarSection.TabIndex = 0;
            this.btnCarSection.Text = "CAR LIST";
            this.btnCarSection.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnCarSection.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(53)))), ((int)(((byte)(81)))));
            this.panel1.Controls.Add(this.btnLogOut);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1322, 40);
            this.panel1.TabIndex = 1;
            // 
            // btnLogOut
            // 
            this.btnLogOut.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnLogOut.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnLogOut.FlatAppearance.BorderSize = 2;
            this.btnLogOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogOut.Font = new System.Drawing.Font("Verdana", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnLogOut.ForeColor = System.Drawing.Color.White;
            this.btnLogOut.Location = new System.Drawing.Point(1222, 0);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(100, 40);
            this.btnLogOut.TabIndex = 0;
            this.btnLogOut.Text = "WYLOGUJ";
            this.btnLogOut.UseVisualStyleBackColor = true;
            // 
            // btnOrdersList
            // 
            this.btnOrdersList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnOrdersList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOrdersList.Font = new System.Drawing.Font("Fira Code", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnOrdersList.Image = ((System.Drawing.Image)(resources.GetObject("btnOrdersList.Image")));
            this.btnOrdersList.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnOrdersList.Location = new System.Drawing.Point(12, 369);
            this.btnOrdersList.Name = "btnOrdersList";
            this.btnOrdersList.Size = new System.Drawing.Size(626, 300);
            this.btnOrdersList.TabIndex = 0;
            this.btnOrdersList.Text = "ORDERS";
            this.btnOrdersList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnOrdersList.UseVisualStyleBackColor = true;
            // 
            // btnWorkerList
            // 
            this.btnWorkerList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnWorkerList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnWorkerList.Font = new System.Drawing.Font("Fira Code", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnWorkerList.Image = ((System.Drawing.Image)(resources.GetObject("btnWorkerList.Image")));
            this.btnWorkerList.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnWorkerList.Location = new System.Drawing.Point(659, 46);
            this.btnWorkerList.Name = "btnWorkerList";
            this.btnWorkerList.Size = new System.Drawing.Size(651, 300);
            this.btnWorkerList.TabIndex = 0;
            this.btnWorkerList.Text = "WORKER LIST";
            this.btnWorkerList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnWorkerList.UseVisualStyleBackColor = true;
            this.btnWorkerList.Click += new System.EventHandler(this.btnWorkerList_Click);
            // 
            // btnReturnsList
            // 
            this.btnReturnsList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnReturnsList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReturnsList.Font = new System.Drawing.Font("Fira Code", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnReturnsList.Image = ((System.Drawing.Image)(resources.GetObject("btnReturnsList.Image")));
            this.btnReturnsList.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnReturnsList.Location = new System.Drawing.Point(659, 368);
            this.btnReturnsList.Name = "btnReturnsList";
            this.btnReturnsList.Size = new System.Drawing.Size(651, 300);
            this.btnReturnsList.TabIndex = 0;
            this.btnReturnsList.Text = "Car Returns";
            this.btnReturnsList.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.btnReturnsList.UseVisualStyleBackColor = true;
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1322, 691);
            this.Controls.Add(this.btnWorkerList);
            this.Controls.Add(this.btnReturnsList);
            this.Controls.Add(this.btnOrdersList);
            this.Controls.Add(this.btnCarSection);
            this.Controls.Add(this.panel1);
            this.Name = "Menu";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCarSection;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnLogOut;
        private System.Windows.Forms.Button btnOrdersList;
        private System.Windows.Forms.Button btnWorkerList;
        private System.Windows.Forms.Button btnReturnsList;
    }
}

